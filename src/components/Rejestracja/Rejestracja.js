import React from 'react';
import styles from './Rejestracja.css';

import Input from '../UI/Input/Input';

const rejestracja = (props) =>{

    return (
        <div className={styles.container}>
            <div className={styles.row}> 
                <div className={styles.content}>
                    <Input styl={{width: '50%', margin: '0 auto'}}/>
                </div>
            </div>
        </div>
    );
}

export default rejestracja;