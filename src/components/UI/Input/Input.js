import React from 'react';
import styles from './Input.css';
const input = props => {
  return (
    <div className={styles.group} style={{ ...props.styl }}>
      <input value={props.value} onChange={props.change} required />
      <span className={styles.bar} />
      <label>{props.placeholder} </label>
    </div>
  );
};

export default input;
