import React from 'react';
import styles from './Podpowiedzi.css';
import Button from '../Button/Button';

import Input from '../Input/Input';

const podpowiedzi = props => {
  let podpowiedzi = null;
  console.log('PropsJestCos', props.jestCos);
  if (props.jestCos) {
    console.log(props.podpowiedzi);
    podpowiedzi = (
      <ul className={styles.Lista}>
        {props.podpowiedzi.map(elm => {
          return (
            <li
              key={elm.name}
              onClick={() => props.hadnleClickPodpowiedz(elm)}
              className={styles.podpowiedz}
            >
              {elm.name}
            </li>
          );
        })}
      </ul>
    );
  }

  return (
    <div className={styles.Granice}>
      <div className={styles.bundle}>
        <Input value={props.value} change={props.change} required
          placeholder={props.placeholder}
        />

        <Button
          click={props.click}
          disabled={!props.jestCos}
          style={{ flexBasis: '20%', marginLeft: '6px' }}
        >
          Dodaj
        </Button>
      </div>
      {props.children ? props.children : podpowiedzi}
    </div>
  );
};

export default podpowiedzi;
