import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Navigation.css';

const navigation = props => {
  

  let style=styles.Navigation
  if(props.typ){
      style=[styles.Navigation, styles[props.typ]].join(' ');
  }
  
  return(  <div className={style}>
    <NavLink to={props.to} activeClassName={styles.active} exact={props.exact}>
      {props.children}
    </NavLink>
  </div>
  );
};

export default navigation;
