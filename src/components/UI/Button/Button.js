import React from 'react';

import styles from './Button.css';

const button = props => (
  <button
    disabled={props.disabled}
    className={styles.Button}
    onClick={props.click}
    style={{
      ...props.style,
      cursor: props.disabled ? 'not-allowed' : 'initial'
    }}
  >
    {props.children}
  </button>
);

export default button;
