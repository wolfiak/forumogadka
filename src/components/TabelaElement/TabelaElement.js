import React from 'react';
import styles from './TabelaElement.css';
import { Draggable } from 'react-beautiful-dnd';





const tabelaElement = (props) => {
    

    return(
            <Draggable draggableId={props.id} index={props.index} >
            {(provided, snapshot)=>(
                <div>
                    <div ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        className={
                            styles.TabelaElm   
                        }
                        style={
                           {
                                    backgroundColor:  props.index === 0  ? '#f58905': null,
                                    ...provided.draggableProps.style
                                
                            }
                        }
                        >
                        <span className={styles.numer}>#{props.index+1}</span>
                        <span className={styles.tekst}> {props.children}</span>
                        <span className={styles.autor}>Qbot</span>
                    
                    </div>
                    {provided.placeholder}
                </div>
            )}
            </Draggable>
    );
    
    
    
    }

export default tabelaElement;