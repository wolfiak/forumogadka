import React from 'react';
import styles from './Toolbar.css';
import  Navigation from '../UI/Navigation/Navigation';

const toolbar = props => (
  <header className={styles.Toolbar}>
    <div className={styles.Tytul}>
      <Navigation to="/" typ="Tytul" exact>Forumogadka Project</Navigation>
    </div>
    <div className={styles.Nawigacja}>
        <Navigation to="/logowanie">Zaloguj</Navigation>
        <Navigation to="/rejestracja">Rejestracja</Navigation>
    </div>
    
  </header>
);

export default toolbar;
