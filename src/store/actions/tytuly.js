import * as actionTypes from './actionTypes';
import axios from '../../axios/axios-firebase';
import reqwest from 'reqwest';

let czas;

export const fetchTytulyInit= ()=> {
    return {
        type: actionTypes.FETCH_TYTULY_INIT
    }
}
export const fetchTytulySuccess= (dane)=>{
    return {
        type: actionTypes.FETCH_TYTULY_SUCCESS,
        dane: dane
    }
}
export const fetchTytulyFaild= (err) =>{
    return{
        type: actionTypes.FETCH_TYTULY_FAILD,
        error: err
    }
}

export const fetchTytulyAsync= ()=> {
    return dispatch => {
        dispatch(fetchTytulyInit());
        axios.get('/tytuly.json')
            .then(res=>{

                console.log('[Reducer: tytuly fetchTytulyAsync]: ',res);
                let tabela=[];
                for(let obiekt in res.data){
                    const obiekto=res.data[obiekt]
                    console.log(res.data[obiekt]);
                    
                    tabela.push({
                        id: obiekto.id ? obiekto.id : obiekt,
                        name: obiekto.name
                    })
                }
                console.log("TABLEKA: ",tabela);
                dispatch(fetchTytulySuccess(tabela));
            })
            .catch(err=>{
                dispatch(fetchTytulyFaild(err));
            });
    };
};

export const fetchPodpowiedziInit= ()=> {
    return {
        type: actionTypes.FETCH_PODPOWIEDZI_INIT

    }
}

export const fetchPodpowiedziSuccess= (data)=> {
    return {
        type: actionTypes.FETCH_PODPOWIEDZI_SUCCESS,
        podpowiedzi: data
    }
}

export const fetchPodpowiedziFaild= ()=> {
    return {
        type: actionTypes.FETCH_PODPOWIEDZI_FAILD
    }
}

export const fetchPodpowiedziAsync= (fraza)=> {
    return dispatch => {
        dispatch(fetchPodpowiedziInit());
         clearTimeout(czas);
         czas=setTimeout(()=>{

          
           reqwest.compat({
               url: `https://www.giantbomb.com/api/games?api_key=7d8302b61012b1964edf5bcc8dfb260ed7004481&filter=name:${fraza}&limit=5&format=jsonp&field_list=name,platforms&sort=date_added:desc&json_callback=bar`,
               dataType: 'jsonp',
               jsonp: 'json_callback',
               jsonpCallback: 'bar',
               success: function(res){
                   console.log(res);
                   const tabelka=[];
                   for(let n of res.results){
                        let platformy=[];
                        if(res.results.platforms){
                        for(let m of res.results.platforms){
                            platformy.push({
                                name: m.name
                            })
                        }
                        }
                        tabelka.push({
                            name: n.name,
                            platformy: platformy ? platformy : null
                        });
                    }
                console.log("[FetchPodpowiedziAsync] Wynik: ",tabelka);
                dispatch(fetchPodpowiedziSuccess(tabelka));
               }

           });
        },500)
    }
}

export const postTytulInit = ()=>{
    return{
        type: actionTypes.POST_TYTULY_INIT
    }
}
export const postTytulSuccess = (data)=>{
    return {
        type: actionTypes.POST_TYTULY_SUCCESS,
        data: data
    }
}
export const postTytulFaild = () =>{
    return {
        type: actionTypes.POST_TYTULY_FAILD
    }
}
export const postTytulAsync= (post)=>{
    return dispatch =>{
        dispatch(postTytulInit());
        axios.post('/tytuly.json',post)
            .then(res=>{
                console.log(res);
                dispatch(postTytulSuccess({
                    id: res.data.name,
                    name: post.name,
                    platformy: post.platformy
                }))
            })
            .catch(err=>{
                console.log('ERR: ',err);
                dispatch(postTytulFaild())
            })
    }
}

export const updateTytulInit=()=>{
    return {
        type: actionTypes.UPDATE_TYTULY_INIT
    }
}
export const updateTytulSuccess= (data)=>{
    return {
        type: actionTypes.UPDATE_TYTULY_SUCCESS,
        data: data
    }
}
export const updateTytulFaild= ()=>{
    return {
        type: actionTypes.UPDATE_TYTULY_FAILD
    }
}
export const updateTytulyAsync= (post)=>{
    return dispatch => {
        console.log('[UpdateTytuly]')
        dispatch(updateTytulInit());
        axios.put('/tytuly.json',post)
            .then(res=>{
                    console.log('RESPOND UPDATE TYTUL ASYNC',res);
                    dispatch(updateTytulSuccess(res.data));
                })
            .catch(err=>{
                dispatch(updateTytulFaild(err))
            })
    }
}

