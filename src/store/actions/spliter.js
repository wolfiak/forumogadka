export {
    fetchTytulyAsync,
    fetchPodpowiedziAsync,
    postTytulAsync,
    updateTytulyAsync
} from './tytuly'