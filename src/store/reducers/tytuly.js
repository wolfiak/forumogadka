import * as actionTypes from '../actions/actionTypes';

const initialState={
    tytuly: [],
    loading: false,
    loadingPodpowiedzi: false,
    podpowiedzi: []
}

const reducer = (state= initialState, action) =>{

    switch(action.type){

        case(actionTypes.FETCH_TYTULY_INIT): {
            return{
                ...state,
                loading: true
            }
        }
        case(actionTypes.FETCH_TYTULY_SUCCESS):{
            return{
                ...state,
                tytuly: action.dane,
                loading: false
            }
        }
        case(actionTypes.FETCH_TYTULY_FAILD):{
            return{
                ...state,
                loading: false
            }
        }
        case(actionTypes.FETCH_PODPOWIEDZI_INIT):{
            return{
                ...state,
                loadingPodpowiedzi: true

            }
        }
        case(actionTypes.FETCH_PODPOWIEDZI_SUCCESS): {
            return{
                ...state,
                loadingPodpowiedzi: false,
                podpowiedzi: action.podpowiedzi
            }
        }
        case(actionTypes.FETCH_PODPOWIEDZI_FAILD): {
            return{
                ...state,
                loadingPodpowiedzi: false
            }
        }
        case(actionTypes.POST_TYTULY_INIT): {
            return{
                ...state,

            }
        }
        case(actionTypes.POST_TYTULY_SUCCESS): {
            return {
                ...state,
                tytuly: state.tytuly.concat(action.data)
            }
        }
        case(actionTypes.POST_TYTULY_FAILD): {
            return {
                ...state
            }
        }
        case(actionTypes.UPDATE_TYTULY_INIT):{
            return {
                ...state,
                loading: true
            }
        }
        case(actionTypes.UPDATE_TYTULY_SUCCESS):{
            return {
                ...state,
                tytuly: action.data,
                loading: false
            }
        }
        case(actionTypes.UPDATE_TYTULY_FAILD):{
            return {
                ...state,
                loading: false
            }
        }
        default: {
            return state;
        }
    }
}

export default reducer;