import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

import Layout from './containers/Layout/Layout';
import Tabela from './containers/Tabela/Tabela';
import Logowanie from './components/Logowanie/logowanie';
import Rejestracja from './components/Rejestracja/Rejestracja';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route path="/logowanie" component={Logowanie} />
            <Route path="/rejestracja" component={Rejestracja} />
            <Route path="/" exact component={Tabela} />
            <Redirect to="/" />
          </Switch>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default App;
