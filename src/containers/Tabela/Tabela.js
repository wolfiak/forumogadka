import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import styles from './Tabela.css';
import { Droppable } from 'react-beautiful-dnd';
import { connect } from 'react-redux';
import Element from '../../components/TabelaElement/TabelaElement';
import Spinner from '../../components/UI/Spinner/Spinner';
import Spinner2 from '../../components/UI/Spinner2/Spinner';
import Podpowiedzi from '../../components/UI/Podpowiedzi/Podpowiedzi';
import * as actions from '../../store/actions/spliter';

import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/fontawesome-free-solid';

class Tabela extends Component {
  state = {
    jestCos: false,
    wartosc: '',
    elm: null,
    showGrabage: false
  };

  componentDidMount() {
    this.props.onFetchTytuly();
  }

  onDragStart = () => {
    console.log('STart');
    this.setState({
      showGrabage: true
    });
  };

  onDragUpdate = () => {
    console.log('Update');
  };

  onDragEnd = res => {
    const tabela = Array.from(this.props.tytuly);
    if (res.destination.droppableId === 'kosz') {
      tabela.forEach((elm, index) => {
        if (elm.id === res.draggableId) {
          tabela.splice(index, 1);
        }
      });
      const nTab = tabela.map((elm, index) => {
        console.log('ELEMENTY W MAP:', elm);
        return {
          ...elm,
          placment: index
        };
      });
      console.log('nTab na poczatku:', nTab);
      this.props.onUpdatePlacment(nTab);
    } else {
      console.log('RES: ', res);
      console.log('Tabela na poczatku:', tabela);
      let usuniety;
      tabela.forEach((elm, index) => {
        if (elm.id === res.draggableId) {
          [usuniety] = tabela.splice(index, 1);
        }
      });
      tabela.splice(res.destination.index, 0, usuniety);

      const nTab = tabela.map((elm, index) => {
        console.log('ELEMENTY W MAP:', elm);
        return {
          ...elm,
          placment: index
        };
      });
      console.log('nTab na poczatku:', nTab);
      this.props.onUpdatePlacment(nTab);
    }
    this.setState({
      showGrabage: false
    });
  };
  inputChangeHandler = event => {
    this.handleValue(event.target.value);
    console.log(event.target.value);
    this.props.onFetchPodpowiedzi(event.target.value);
    this.setState({
      jestCos: event.target.value !== ''
    });
  };
  hadnleClickPodpowiedz = elm => {
    console.log('KLIKO', elm);
    console.log({ ...elm });
    this.handleValue(elm.name);
    this.setState({
      elm: { ...elm }
    });
  };
  hadnleClickDodaj = () => {
    this.props.onPostTytul(this.state.elm);
    this.handleValue('');
  };
  handleValue = wartosc => {
    this.setState({
      wartosc: wartosc,
      jestCos: wartosc === '' ? false : true
    });
  };

  render() {
    console.log(this.props.tytuly);
    const toCos = this.props.tytuly.map((elm, index) => {
      return (
        <Element key={elm.id} id={elm.id} index={index}>
          {elm.name}
        </Element>
      );
    });
    let tabela = (
      <div className={styles.divZtabelka}>
        <Podpowiedzi
          podpowiedzi={this.props.podpowiedzi}
          change={this.inputChangeHandler}
          placeholder="Wrpowadź tytuł do rankingu!"
          jestCos={this.state.jestCos}
          hadnleClickPodpowiedz={this.hadnleClickPodpowiedz}
          value={this.state.wartosc}
          click={this.hadnleClickDodaj}
        >
          {this.props.loadingPodpowiedzi ? <Spinner2 /> : null}
        </Podpowiedzi>
        {!this.state.jestCos ? (
          <DragDropContext
            onDragStart={this.onDragStart}
            onDragUpdate={this.onDragUpdate}
            onDragEnd={this.onDragEnd}
          >
            <Droppable droppableId="dropPoint">
              {(provided, snapshot) => (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                  {toCos}
                </div>
              )}
            </Droppable>
            {this.state.showGrabage ? (
              <Droppable droppableId="kosz">
                {(provided, snapshot) => (
                  <div
                    className={styles.kosz}
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                  >
                    <FontAwesomeIcon
                      className={styles.koszTekst}
                      icon={faTrash}
                    />
                    <p className={styles.koszTekst}>DROP GARBAGE HERE!</p>
                  </div>
                )}
              </Droppable>
            ) : null}
          </DragDropContext>
        ) : null}
      </div>
    );
    if (this.props.loading) {
      tabela = <Spinner />;
    }
    return (
      <div className={styles.container}>
        <div className={styles.row}>
          <div className={styles.jumbotorn}>
            <p className={styles.leadText}>Forumogadka top coś tam</p>
          </div>

          {tabela}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    loading: state.tytuly.loading,
    podpowiedzi: state.tytuly.podpowiedzi,
    loadingPodpowiedzi: state.tytuly.loadingPodpowiedzi,
    tytuly: state.tytuly.tytuly
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchTytuly: () => dispatch(actions.fetchTytulyAsync()),
    onFetchPodpowiedzi: fraza => dispatch(actions.fetchPodpowiedziAsync(fraza)),
    onPostTytul: post => dispatch(actions.postTytulAsync(post)),
    onUpdatePlacment: post => dispatch(actions.updateTytulyAsync(post))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Tabela);
