import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Toolbar from '../../components/Toolbar/Toolbar';
import Stopka from '../../components/Stopka/Stopka';
import Aux from '../../hoc/Auxo';

import styles from './Layout.css';

class Layout extends Component {
  render() {
    let stopka;
    console.log(
      'stopka: ',
      this.props.location.pathname.localeCompare('/rejestracja') !== 0
    );
    if (
      this.props.location.pathname.localeCompare('/rejestracja') !== 0 &&
      this.props.location.pathname.localeCompare('/logowanie') !== 0
    ) {
      stopka = <Stopka />;
    }
    console.log('Historaia', this.props);
    return (
      <Aux>
        <Toolbar />
        <main className={styles.Layout}>{this.props.children}</main>
        {stopka}
      </Aux>
    );
  }
}

export default withRouter(Layout);
