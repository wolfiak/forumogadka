import axios from 'axios';
const firebase=axios.create({
    baseURL: 'https://www.giantbomb.com/api/games',
    responseType: 'json'
});

export default firebase;

