import axios from 'axios';
const firebase=axios.create({
    baseURL: 'https://forumogadka.firebaseio.com/'
});

export default firebase;